#!/usr/bin/python
#This requires the python-twitter library (https://github.com/bear/python-twitter)
import time, sys, string, twitter

DEBUG = False
try:
    import RPi.GPIO as GPIO
except:
    print time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()) + " No GPIO. Going to DEBUG mode."
    DEBUG = True

#TODO: If non-encodable letters are tweeted, tweet back.

#IMPORTANT: This must be set to the PWD of the python script if run as an init service
#import os
#os.chdir("")

if not DEBUG:
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(7, GPIO.OUT)

#open an unbuffered file to log events
log = open("./znjp_bulb.log", "w", 0)

tokens = {}
execfile("tokens.py", tokens)
api = twitter.Api(consumer_key=tokens["consumer_key"], 
                  consumer_secret=tokens["consumer_secret"], 
                  access_token_key=tokens["access_token_key"], 
                  access_token_secret=tokens["access_token_secret"], 
                  sleep_on_rate_limit=True,
                  input_encoding=None)

CODE = {'A': '.-',     'B': '-...',   'C': '-.-.', 
        'D': '-..',    'E': '.',      'F': '..-.',
        'G': '--.',    'H': '....',   'I': '..',
        'J': '.---',   'K': '-.-',    'L': '.-..',
        'M': '--',     'N': '-.',     'O': '---',
        'P': '.--.',   'Q': '--.-',   'R': '.-.',
     	'S': '...',    'T': '-',      'U': '..-',
        'V': '...-',   'W': '.--',    'X': '-..-',
        'Y': '-.--',   'Z': '--..',
        
        '0': '-----',  '1': '.----',  '2': '..---',
        '3': '...--',  '4': '....-',  '5': '.....',
        '6': '-....',  '7': '--...',  '8': '---..',
        '9': '----.' 
        }

DOT_DURATION = .1 #In seconds

def Dot():
    if DEBUG:
        print ".",
    else:
        GPIO.output(7,True)
        time.sleep(DOT_DURATION)
        GPIO.output(7,False)
        time.sleep(DOT_DURATION*3)

def Dash():
    if DEBUG:
        print "-",
    else:
        GPIO.output(7,True)
        time.sleep(DOT_DURATION*3)
        GPIO.output(7,False)
        time.sleep(DOT_DURATION*3)

def main():

    #for each tweet
    for tweet in api.GetUserStream():
        try:
            #Grab the ASCII portion of the tweet.
            full_text = tweet[u'text'].encode('ascii', 'ignore')
        except KeyError:
            continue
        
        #Grab everything that comes after @znjp_bulb
        text = full_text[full_text.find("@znjp_bulb") + len("@znjp_bulb "):]
        log.write(time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()) + " " + text + "\n")

        #If DEBUG, print the tweet for all to see.
        if DEBUG:
            print text + " ",
            for c in text:
                #Make sure it's a Morse-encodable character
                if c not in string.digits + string.letters:
                    continue
                if c is " ":
                    print " "
                for d in CODE[c.upper()]:
                    print d,
                print " ",
            print " "

        #Blink the light
        #For each single flag character
        for c in text:
            #Make sure it's a Morse-encodable character
            if c not in string.digits + string.letters:
                continue
            #Wait between words.
            if c is " ":
                time.sleep(DOT_DURATION*9)
            #Encode a letter
            for d in CODE[c.upper()]:
                if d == ".":
                    Dot()
                else:
                    Dash()
            if DEBUG:
                print " ",
            #Wait between letters.
            time.sleep(DOT_DURATION*3)



if __name__ == "__main__":
    
    while(True):
        try:
            log.write(time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()) + " Starting znjp_bulb.\n")
            main()
        except twitter.error.TwitterError:
            log.write(time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()) + " Twitter Error. Sleeping and will retry in 10 seconds.\n")
            time.sleep(10)
        except (KeyboardInterrupt, SystemExit):
            if not DEBUG:
                GPIO.cleanup()
            log.write(time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()) + " Exiting.\n")
            quit()
        except:
            log.write(time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()) + "Exception: " + str(sys.exc_info()[0]) + "\n")
            
